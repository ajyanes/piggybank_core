/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alcancia.demo.repository;

import com.alcancia.demo.model.Alcancia;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ayanes
 */
@Repository
public interface AlcanciaRepository extends JpaRepository<Alcancia, Long>{
    
    List<Alcancia> findByDenom(String den);
        
}
