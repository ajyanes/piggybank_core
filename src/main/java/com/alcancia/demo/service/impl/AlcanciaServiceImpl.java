/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alcancia.demo.service.impl;

import com.alcancia.demo.mapper.AlcanciaMapper;
import com.alcancia.demo.model.Alcancia;
import com.alcancia.demo.model.AlcanciaDTO;
import com.alcancia.demo.repository.AlcanciaRepository;
import com.alcancia.demo.service.IAlcanciaService;
import java.util.Arrays;
import java.util.List;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ayanes
 */
@Service
public class AlcanciaServiceImpl implements IAlcanciaService {

    String[] valores = {"50", "100", "200", "500", "1000"};

    @Autowired
    AlcanciaRepository alcanciaRepository;

    AlcanciaMapper alcmapper = Mappers.getMapper(AlcanciaMapper.class);

    @Override
    public AlcanciaDTO depositar(AlcanciaDTO adto) {
        Alcancia alc = alcmapper.alcanciaDtoToAlcancia(adto);
        Alcancia res = null;
        AlcanciaDTO resDTO = null;
        boolean validate = validaDenom(alc.getDenom());
        boolean validateCantidad = isNumeric(String.valueOf(alc.getCant()));
        boolean valorPositivo = isPositive(String.valueOf(alc.getCant()));

        if (!validateCantidad) {
            resDTO = AlcanciaDTO.builder().mensaje("Digite una cantidad valida").build();
        }
        
        if (!valorPositivo) {
            resDTO = AlcanciaDTO.builder().mensaje("Digite una cantidad mayor a cero").build();
        }
        
        if (validate && valorPositivo && validateCantidad) {
            res = alcanciaRepository.save(alc);
            resDTO = alcmapper.alcanciaToAlcanciaDTO(res);
        } else {
            resDTO = AlcanciaDTO.builder().mensaje("El valor de la moneda no es valido").build();
        }

        return resDTO;
    }

    @Override
    public int cuentaMonedas() {
        int cuentamonedas = 0;
        AlcanciaDTO resDTO = null;
        List<Alcancia> listResult = alcanciaRepository.findAll();
        for (Alcancia a : listResult) {
            cuentamonedas = cuentamonedas + a.getCant();
        }

        return cuentamonedas;
    }

    @Override
    public int valorAhorrado() {
        int ahorrado = 0;
        List<Alcancia> listResult = alcanciaRepository.findAll();
        for (Alcancia a : listResult) {
            ahorrado = ahorrado + (a.getCant() * Integer.parseInt(a.getDenom()));
        }

        return ahorrado;
    }

    @Override
    public AlcanciaDTO monedasxdenom(String denom) {
        int cuentamonedas = 0;
        List<Alcancia> listResult = alcanciaRepository.findByDenom(denom);
        for (Alcancia a : listResult) {
            cuentamonedas = cuentamonedas + a.getCant();
        }
        AlcanciaDTO resDTO = AlcanciaDTO.builder()
                .denom(denom)
                .cant(cuentamonedas)
                .build();

        return resDTO;
    }

    @Override
    public AlcanciaDTO dineroxdenom(String denom) {
        int dinero = 0;
        List<Alcancia> listResult = alcanciaRepository.findByDenom(denom);
        for (Alcancia a : listResult) {
            dinero = dinero + (a.getCant() * Integer.parseInt(a.getDenom()));
        }
        AlcanciaDTO resDTO = AlcanciaDTO.builder()
                .denom(denom)
                .dinero(dinero)
                .build();

        return resDTO;
    }

    private boolean validaDenom(String denom) {
        return Arrays.asList(valores).contains(denom);
    }

    private boolean isNumeric(String cantidad) {
        try {
            Integer.parseInt(cantidad);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    private boolean isPositive(String cantidad) {
        if (Integer.parseInt(cantidad) < 0) {
            return false;
        } else {
            return true;
        }
    }

}
