/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alcancia.demo.service;

import com.alcancia.demo.model.AlcanciaDTO;

/**
 *
 * @author ayanes
 */
public interface IAlcanciaService {
    
    AlcanciaDTO depositar(AlcanciaDTO adto);
    int cuentaMonedas();
    int valorAhorrado();
    AlcanciaDTO monedasxdenom(String denom);
    AlcanciaDTO dineroxdenom(String denom);
}
