/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alcancia.demo.controller;

import com.alcancia.demo.model.AlcanciaDTO;
import com.alcancia.demo.service.IAlcanciaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ayanes
 */
@RestController
@RequestMapping("/moneybox")
public class AlcanciaController {
    
    @Autowired
    IAlcanciaService alcanciaService;
    
    @PostMapping("/depositar")
    public AlcanciaDTO guardar(@RequestBody final AlcanciaDTO adto){
        return alcanciaService.depositar(adto);
    }
    
    @GetMapping("/conteo")
    public int contarMonedas(){
        return alcanciaService.cuentaMonedas();
    }
    
    @GetMapping("/ahorro")
    public int verAhorro(){
        return alcanciaService.valorAhorrado();
    }
    
    @GetMapping("/monedasdenom/{denom}")
    public AlcanciaDTO getMonedasByDenom(@PathVariable String denom){
        return alcanciaService.monedasxdenom(denom);
    }
    
    @GetMapping("/dinerodenom/{denom}")
    public AlcanciaDTO getDineroByDenom(@PathVariable String denom){
        return alcanciaService.dineroxdenom(denom);
    }
    
}
