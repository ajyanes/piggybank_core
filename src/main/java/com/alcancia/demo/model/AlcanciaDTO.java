/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alcancia.demo.model;

import lombok.Builder;
import lombok.Data;

/**
 *
 * @author ayanes
 */
@Data
@Builder
public class AlcanciaDTO {
    Long id;
    String denom;
    int cant;
    int dinero;
    String mensaje;
}
