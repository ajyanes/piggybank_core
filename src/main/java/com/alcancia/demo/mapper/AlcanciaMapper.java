/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alcancia.demo.mapper;

import com.alcancia.demo.model.Alcancia;
import com.alcancia.demo.model.AlcanciaDTO;
import org.mapstruct.Mapper;

/**
 *
 * @author ayanes
 */
@Mapper
public interface AlcanciaMapper {
    
    AlcanciaDTO alcanciaToAlcanciaDTO(Alcancia alcancia);
    
    Alcancia alcanciaDtoToAlcancia(AlcanciaDTO alcanciaDTO);
}
