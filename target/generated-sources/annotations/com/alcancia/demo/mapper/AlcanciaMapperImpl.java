package com.alcancia.demo.mapper;

import com.alcancia.demo.model.Alcancia;
import com.alcancia.demo.model.Alcancia.AlcanciaBuilder;
import com.alcancia.demo.model.AlcanciaDTO;
import com.alcancia.demo.model.AlcanciaDTO.AlcanciaDTOBuilder;
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-10-11T18:32:19-0500",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 11.0.5 (Oracle Corporation)"
)
public class AlcanciaMapperImpl implements AlcanciaMapper {

    @Override
    public AlcanciaDTO alcanciaToAlcanciaDTO(Alcancia alcancia) {
        if ( alcancia == null ) {
            return null;
        }

        AlcanciaDTOBuilder alcanciaDTO = AlcanciaDTO.builder();

        alcanciaDTO.id( alcancia.getId() );
        alcanciaDTO.denom( alcancia.getDenom() );
        alcanciaDTO.cant( alcancia.getCant() );

        return alcanciaDTO.build();
    }

    @Override
    public Alcancia alcanciaDtoToAlcancia(AlcanciaDTO alcanciaDTO) {
        if ( alcanciaDTO == null ) {
            return null;
        }

        AlcanciaBuilder alcancia = Alcancia.builder();

        alcancia.id( alcanciaDTO.getId() );
        alcancia.denom( alcanciaDTO.getDenom() );
        alcancia.cant( alcanciaDTO.getCant() );

        return alcancia.build();
    }
}
